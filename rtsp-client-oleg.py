# from twisted.internet import protocol, reactor, endpoints
#
#
# class Echo(protocol.Protocol):

#     def dataReceived(self, data):
#         self.transport.write(data)
#
#
# class EchoFactory(protocol.Factory):
#     def buildProtocol(self, addr):
#         return Echo()
#
#
# endpoints.serverFromString(reactor,"tcp:1234").listen(EchoFactory())
# reactor.run()
from twisted.internet import protocol, reactor
from base64 import b64encode

host = '109.74.133.138'
port = 558
# host = 'localhost'
# port = 777


class Twist_client(protocol.Protocol):
    def __init__(self):
        self.config = {
            'request': '/jpeg',
            'login': 'admin',
            'pass': '19922002t',
            'ip': '109.74.133.138',
            'port': '558', # non-standard port - standard :554
            # 'udp_port': 41760
        }
    # Отправка сообщения с response printing
    def sendData(self):
        # to_send = input('write message: ')
        # to_send = 'MESSAGE'

        # authstring = 'Authorization: Basic ' + self.config['login'] + ':' + self.config['pass'] + 'RVi/1/1' + '\r\n'
#         to_send = """\
# OPTIONS rtsp://""" + self.config['ip'] + self.config['request'] + """ RTSP/1.0\r
# """  """CSeq: 1\r
# User-Agent: Python MJPEG Client\r
# \r
# """
        to_send = "OPTIONS rtsp://" + self.config['ip'] + self.config['request'] + \
                  " RTSP/1.0\r\nCSeq: 1\r\nUser-Agent: Python MJPEG Client\r\n\r\n"

#         to_send = """\
# DESCRIBE rtsp://""" + self.config['ip'] + self.config['request'] + """ RTSP/1.0\r
# CSeq: 2\r
# Accept: application/sdp\r
# User-Agent: Python MJPEG Client\r
# \r
# """

        if to_send:
            print(to_send, type(to_send))
            self.transport.write(to_send.encode())
        else:
            self.transport.loseConnection()

    def connectionMade(self):
        print("connecting...")
        self.sendData()

    def dataReceived(self, data):

        print(data.decode(), type(data))
        self.transport.loseConnection()


class Twist_Factory(protocol.ClientFactory):
    protocol = Twist_client

    def clientConnectionFailed(self, connector, reason):
        print('connection failed:', reason.getErrorMessage())
        reactor.stop()

    def clientConnectionLost(self, connector, reason):
        print('connection lost:', reason.getErrorMessage())
        reactor.stop()


factory = Twist_Factory()
reactor.connectTCP(host, port, factory)
reactor.run()
#
# 01: OPTIONS rtsp://192.168.0.254/jpeg RTSP/1.0
# 02: CSeq: 1
# 03: User-Agent: VLC media player (LIVE555 Streaming Media v2008.07.24)


# from twisted.internet import protocol, reactor
# # 2, определить класс протокола клиента
# class TSClientProtocol(protocol.Protocol):
# 	def sendData(self):
# 		data = input("> ")
# 		if data:
# 			self.transport.write(data.encode())
# 		else:
# 			self.transport.loseConnection()
#
# 	def connectionMade(self):
# 		self.sendData()
#
# 	def dataReceived(self, data):
# 		recData = data.decode() # Конвертировать двоичные данные в строковые данные.
# 		print(f"Данные получены с сервера: ${recData}")
# 		# self.sendData()
# # 3, определить класс фабрики клиента
# class TSClientFactory(protocol.ClientFactory):
# 	protocol = TSClientProtocol
# 	clientConnectionLost = clientConnectionFailed = lambda self, connector, reason:reactor.stop()
# # 4, используйте реактор, чтобы начать соединение
# reactor.connectTCP(HOST, PORT, TSClientFactory())
# reactor.run() # Стартовый цикл