# Rtsp Client Twisted

## Cоздать и активировать виртуальное окружение:
```python
python3 -m venv venv

source venv/bin/activate

python3 -m pip install --upgrade pip
```


### Установить зависимости из файла requirements.txt:
```python
pip3 install -r requirements.txt
pip3 install opencv-python
```

### Record library tu requirements.txt
```python
pip3 freeze > requirements.txt
```


### start project
```python
python3 12-livestream-Matiya.py 
```