## Cоздать и активировать виртуальное окружение:
```python
python3 -m venv venv

source venv/bin/activate

----deactivate-----

python3 -m pip install --upgrade pip
```


### Установить зависимости из файла requirements.txt:
```python
pip3 install -r requirements.txt
pip3 install opencv-python
```

### Record library tu requirements.txt
```python
pip3 freeze > requirements.txt
```

```python
pip3 install pybase64
```


### start project
```python
python3 12-livestream-Matiya.py 
```
