# Стабильно на python 2.7
# Скачать twisted и необходимый zope
# twistedmatrix.com/trac/wiki/Downloads
# pypi.python.org/pypi/zope.interface/4.1.2

# Twisted - управляемая событиями(event) структура
# Событиями управляют функции – event handler
# Цикл обработки событий отслеживает события и запускает соответствующие event handler
# Работа цикла лежит на объекте reactor из модуля twisted.internet

# Модуль socketserver для сетевого программирования
from twisted.internet import protocol, reactor


class Twist(protocol.Protocol):

    # Событие connectionMade срабатывает при соединении
    def connectionMade(self):
        print('connection success!')

    # Событие dataReceived - получение и отправление данных
    def dataReceived(self, data):
        recivedData = data.decode()
        print(recivedData)

        # transport.write - отправка сообщения
        self.transport.write('Hello from server!'.encode())

    # Событие connectionLost срабатывает при разрыве соединения с клиентом
    def connectionLost(self, reason):
        print('Connection lost!')



# Конфигурация поведения протокола описывается в – классе Factory из twisted.internet.protocol.Factory
factory = protocol.Factory()
factory.protocol = Twist
print('wait...')

reactor.listenTCP(777, factory)
reactor.run()

#
# # ------------https://russianblogs.com/article/231756353/-------------
# # 1, библиотека, на которую нужно ссылаться
# PORT = 777
#
# from twisted.internet import protocol, reactor
# from twisted.internet.endpoints import TCP4ServerEndpoint
# # TCP4ServerEndpoint требуется win32api
# from time import ctime
#
#
# # 2, определить класс протокола на стороне сервера
# class TSServerProtocol(protocol.Protocol):
#     """
#      Протокол на стороне сервера
#      Каждое клиентское соединение соответствует экземпляру.
#     """
#
#     def __init__(self):
#         self.clientInfo = ""  # clientInfo сохранит информацию о клиентском соединении.
#
#     def connectionMade(self):
#         self.clientInfo = self.transport.getPeer()
#         print(f"Соединение с ${self.clientInfo}")
#
#     def dataReceived(self, data):
#         recData = data.decode()
#         print(f"Получены данные от ${self.clientInfo}, ${recData}")
#         rep = f"${ctime()}, ${recData})"
#         self.transport.write(rep.encode())
#
#
# # 3. Определите класс фабрики на стороне сервера
# class TSServerFactory(protocol.Factory):
#     def buildProtocol(self, addr):
#         return TSServerProtocol()
#
#
# # 4, используйте реактор для запуска мониторинга порта
# endpoint = TCP4ServerEndpoint(reactor, PORT)
# endpoint.listen(TSServerFactory())
#
# print("Ожидание подключения клиента")
# reactor.run()
