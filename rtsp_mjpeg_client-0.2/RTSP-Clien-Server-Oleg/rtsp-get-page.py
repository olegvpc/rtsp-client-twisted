#!/usr/bin/env python
# -*- coding:utf-8 -*-
from twisted.internet import defer  # Метод обработки функции обратного вызова
from twisted.web.client import getPage
from twisted.internet import reactor


def one_done(arg):
    print(arg)


def all_done(arg):
    print('done')
    reactor.stop()  # Стоп цикл


@defer.inlineCallbacks  # Callback декоратор
def task(url):
    res = getPage(bytes(url, encoding='utf8'))  # Отправить запрос Http
    res.addCallback(one_done)  # Обратный звонок
    yield res


url_list = [
    'http://www.baidu.com',
    'http://www.so.com',
    'http://www.sogou.com',
]

defer_list = []  # [Special, special, special (уже отправлен запрос на URL)]
for url in url_list:
    v = task(url)
    defer_list.append(v)
d = defer.DeferredList(defer_list)
d.addBoth(all_done)